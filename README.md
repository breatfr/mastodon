# [![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/breatfr) <a href="https://www.paypal.me/breat"><img src="https://github.com/andreostrovsky/donate-with-paypal/raw/master/blue.svg" alt="PayPal" height="30"></a>
[mastodon](https://mastodon.social/) website is more suitable for wide screens.
## Preview
![Preview](https://gitlab.com/breatfr/mastodon/-/raw/main/docs/preview.jpg)

## List of available customizations
- custom font size
- wide mode

<details>
  <summary><h2>List of supported instances</h2></summary>
  if you need add your instance, contact me.
  <h3>0-9</h3>
  <ul>
    <li>0day.agency</li>
    <li>1password.social</li>
    <li>7nw.eu</li>
  </ul>

  <h3>A</h3>
  <ul>
    <li>aleph.land</li>
    <li>alex73630.xyz</li>
    <li>animalliberation.social</li>
    <li>awoo.space</li>
  </ul>

  <h3>B</h3>
  <ul>
    <li>ballpointcarrot.net</li>
    <li>beekeeping.ninja</li>
    <li>birdon.social</li>
    <li>blorbo.social</li>
    <li>bonn.social</li>
    <li>bytestemplar.com</li>
  </ul>

  <h3>C</h3>
  <ul>
    <li>c.im</li>
    <li>chaos.social</li>
    <li>convo.casa</li>
    <li>constructed.space</li>
    <li>crime.team</li>
  </ul>

  <h3>D</h3>
  <ul>
    <li>dancingbanana.party</li>
    <li>design.vu</li>
    <li>det.social</li>
    <li>digitalhumanities.club</li>
    <li>diskseven.com</li>
    <li>dissidence.ovh</li>
  </ul>

  <h3>E</h3>
  <ul>
    <li>elouworld.org</li>
    <li>epiktistes.com</li>
    <li>expressional.social</li>
  </ul>

  <h3>F</h3>
  <ul>
    <li>famichiki.jp</li>
    <li>fandom.ink</li>
    <li>fern.surgeplay.com</li>
    <li>floss.social</li>
    <li>fosstodon.org</li>
  </ul>

  <h3>G</h3>
  <ul>
    <li>gamedev.place</li>
    <li>gay.crime.team</li>
    <li>gestaltzerfall.net</li>
    <li>good-dragon.com</li>
    <li>gougere.fr</li>
  </ul>

  <h3>H</h3>
  <ul>
    <li>hachyderm.io</li>
    <li>hear-me.social</li>
    <li>hostux.social</li>
  </ul>

  <h3>I</h3>
  <ul>
    <li>icosahedron.website</li>
    <li>ieji.de</li>
    <li>im-in.space</li>
    <li>imirhil.fr</li>
    <li>indieweb.social</li>
    <li>indigo.zone</li>
    <li>infinimatix.net</li>
    <li>interlinked.me</li>
  </ul>

  <h3>K</h3>
  <ul>
    <li>kagrumez.lerk.io</li>
  </ul>

  <h3>L</h3>
  <ul>
    <li>linuxrocks.online</li>
    <li>lkw.tf</li>
    <li>llamasweet.tech</li>
    <li>lor.sh</li>
    <li>lou.lt</li>
    <li>louiz.org</li>
  </ul>

  <h3>M</h3>
  <ul>
    <li>m4570.xyz</li>
    <li>maly.io</li>
    <li>manx.social</li>
    <li>manowar.social</li>
    <li>mas.to</li>
    <li>mastodon.app.uk</li>
    <li>mastodon.art</li>
    <li>mastodon.brussels</li>
    <li>mastodon.cc</li>
    <li>mastodon.cloud</li>
    <li>mastodon.club</li>
    <li>mastodon.com.pl</li>
    <li>mastodon.cx</li>
    <li>mastodon.fun</li>
    <li>mastodon.gamedev.place</li>
    <li>mastodon.gougere.fr</li>
    <li>mastodon.host</li>
    <li>mastodon.land</li>
    <li>mastodon.llamasweet.tech</li>
    <li>mastodon.ml</li>
    <li>mastodon.network</li>
    <li>mastodon.ninetailed.uk</li>
    <li>mastodon.nuzgo.net</li>
    <li>mastodon.online</li>
    <li>mastodon.partipirate.org</li>
    <li>mastodon.social</li>
    <li>mastodon.systemlab.fr</li>
    <li>mastodon.technology</li>
    <li>mastodon.top</li>
    <li>mastodon.world</li>
    <li>mastodon.xyz</li>
    <li>mastodonapp.uk</li>
    <li>mast3k.interlinked.me</li>
    <li>masto.ai</li>
    <li>masto.raildecake.fr</li>
    <li>masto.razrnet.fr</li>
    <li>masto.themimitoof.fr</li>
    <li>mastodon-uk.net</li>
    <li>memetastic.space</li>
    <li>meow.social</li>
    <li>mst3k.interlinked.me</li>
    <li>mstdn.business</li>
    <li>mstdn.io</li>
    <li>mstdn.party</li>
    <li>mstdn.plus</li>
    <li>mstdn.social</li>
  </ul>

  <h3>N</h3>
  <ul>
    <li>nasqueron.org</li>
    <li>nerdculture.de</li>
    <li>neumastodon.com</li>
    <li>ninetailed.uk</li>
    <li>niu.moe</li>
    <li>nowa.re</li>
    <li>nuzgo.net</li>
  </ul>

  <h3>O</h3>
  <ul>
    <li>oc.todon.fr</li>
    <li>octodon.social</li>
    <li>off-the-clock.us</li>
    <li>ohai.social</li>
    <li>opalstack.social</li>
    <li>opencoaster.net</li>
    <li>oulipo.social</li>
  </ul>

  <h3>P</h3>
  <ul>
    <li>partipirate.org</li>
    <li>persiansmastodon.com</li>
    <li>planetearth.social</li>
  </ul>

  <h3>R</h3>
  <ul>
    <li>reveal.today</li>
    <li>rich.gop</li>
  </ul>

  <h3>S</h3>
  <ul>
    <li>securitymastod.one</li>
    <li>share.elouworld.org</li>
    <li>soc.louiz.org</li>
    <li>social.0day.agency</li>
    <li>social.alex73630.xyz</li>
    <li>social.ballpointcarrot.net</li>
    <li>social.bbc</li>
    <li>social.bytestemplar.com</li>
    <li>social.diskseven.com</li>
    <li>social.gestaltzerfall.net</li>
    <li>social.imirhil.fr</li>
    <li>social.lkw.tf</li>
    <li>social.lou.lt</li>
    <li>social.nasqueron.org</li>
    <li>social.nowa.re</li>
    <li>social.targaryen.house</li>
    <li>social.vivaldi.net</li>
    <li>social.wxcafe.net</li>
    <li>socially.constructed.space</li>
    <li>status.dissidence.ovh</li>
    <li>stranger.social</li>
    <li>systemlab.fr</li>
  </ul>

  <h3>T</h3>
  <ul>
    <li>targaryen.house</li>
    <li>toot.community</li>
    <li>toot.io</li>
  </ul>

  <h3>U</h3>
  <ul>
    <li>universeodon.com</li>
  </ul>

  <h3>W</h3>
  <ul>
    <li>wehavecookies.social</li>
    <li>wxcafe.net</li>
  </ul>

  <h3>Y</h3>
  <ul>
    <li>yttrx.com</li>
  </ul>
</details>


## How to use in few steps
1. Install Stylus browser extension
    - Chromium based browsers link: https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne
        - Brave
        - Chromium
        - Google Chrome
        - Iridium Browser
        - Microsoft Edge
        - Opera
        - Opera GX
        - SRWare Iron
        - Ungoogled Chromium
        - Vivaldi
        - Yandex Browser
        - many more
    - Firefox based browsers link: https://addons.mozilla.org/firefox/addon/styl-us/
        - Mozilla Firefox
        - Mullvad Browser
        - Tor Browser
        - Waterfox
        - many more

2. Go on [UserStyles.world](https://userstyles.world/style/16900) website and click on `Install` under the preview picture or open the [GitLab version](https://gitlab.com/breatfr/mastodon/-/raw/main/css/mastodon-responsive-customizations.user.css).

3. To update the theme, open the `Stylus Management` window and click on `Check for update` and follow the instructions or just wait 24h to automatic update

4. Enjoy :)
# [![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/breatfr) <a href="https://www.paypal.me/breat"><img src="https://github.com/andreostrovsky/donate-with-paypal/raw/master/blue.svg" alt="PayPal" height="30"></a>
